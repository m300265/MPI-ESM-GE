#!/bin/bash

# performs integrity check on output (checks if files are present, mainly for BOT and ATM files)

# directory where experiments are stored, i.e. parent directory of ensemble realisations
expdir=/work/mh0033/m300265/MPI-ESM-GE/onepercentco2
# prefix common to all experiments
prefix=oneperc
suffix=

# id of first and last experiment (combines lkm and mb 1% runs)
run_0=1
run_n=100

# startyear and endyear of experiment
startyear=1850
endyear=2005

miss_counter_all=0
#============= Loop through each run
for rrr in $(seq $run_0 $run_n); do
	expid=${prefix}$(printf "%04d" $rrr)${suffix}
    echo "================================="
    echo ${expid}
    if [ $rrr -ge 69 ]; then
        endyear=1999 # output for most mb runs only available until 1999
    fi
    echamdir=${expdir}/${expid}/outdata/echam6
    # check for BOT files
    miss_counter=0
    for yyyy in $(seq $startyear $endyear); do
    	outfile=${echamdir}/${expid}_echam6_BOT_mm_${yyyy}.grb
    	if [ ! -f $outfile ]; then
    		echo BOT missing: ${yyyy}
    		((miss_counter++))
            ((miss_counter_all++))
    	fi
    done
    if [ $miss_counter -eq 0 ]; then
    		echo ${expid} BOT ok.
    	else
    		echo ${expid} missing BOT files: ${miss_counter}
    fi
    # check for ATM files
    miss_counter=0
    for yyyy in $(seq $startyear $endyear); do
    	outfile=${echamdir}/${expid}_echam6_ATM_mm_${yyyy}.grb
    	if [ ! -f $outfile ]; then
    		echo ATM missing: ${yyyy}
    		((miss_counter++))
            ((miss_counter_all++))
    	fi
    done
    if [ $miss_counter -eq 0 ]; then
    		echo ${expid} ATM ok.
    	else
    		echo ${expid} missing ATM files: ${miss_counter}
    fi
done

    if [ $miss_counter_all -eq 0 ]; then
        echo Output complete, no missing files found.
    else
        echo Output incomplete, total number of missing files: $miss_counter_all
    fi
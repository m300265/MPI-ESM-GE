#!/bin/bash

# performs integrity check on output (checks if model output is missing for some years and rerun is needed)


# directory where experiments are stored, i.e. parent directory of ensemble realisations
expdir=/work/bm0976/m300497/MPIESM/GLENSv1/output/experiments
# prefix common to all experiments
prefix=cr
suffix=_rcp85

# id of first and last experiment
run_0=131 #101
run_n=140 #166

# startyear and endyear of experiment
startyear=2006 #1850
endyear=2099 #1999

# if hamocc writes monthly 3d output, set this to 1 (0 otherwise)
hamocc_monthly=1

# echo -ne '#####                     (33%)\r'
# sleep 1
# echo -ne '#############             (66%)\r'
# sleep 1
# echo -ne '#######################   (100%)\r'
# echo -ne '\n'

miss_counter_restart_all=0 # keeps track of how many experiments have missing restart files
miss_counter_pp_all=0 # keeps track of how many experiments have missing BOT or ATM files

#============= Loop through each run
echo " "
for rrr in $(seq $run_0 $run_n); do
	expid=${prefix}$(printf "%04d" $rrr)${suffix}
    echo "================================="
    echo checking: ${expid}
    echo " "

    outdir=${expdir}/${expid}/outdata
    echamdir=${outdir}/echam6
    restartdir=${expdir}/${expid}/restart

    ###################
    # check restart:

    miss_counter_restart_echam=0
    miss_list_restart_echam=" "
    miss_counter_restart_hamocc=0
    miss_list_restart_hamocc=" "
    miss_counter_restart_jsbach=0
    miss_list_restart_jsbach=" "
    miss_counter_restart_mpiom=0
    miss_list_restart_mpiom=" "
    miss_counter_restart_oasis=0
    miss_list_restart_oasis=" "
    for yyyy in $(seq $startyear $endyear); do
        # ECHAM
        outfile_accw=${restartdir}/echam6/restart_${expid}_accw_${yyyy}1231.nc
        outfile_co2=${restartdir}/echam6/restart_${expid}_co2_${yyyy}1231.nc
        outfile_echam=${restartdir}/echam6/restart_${expid}_echam_${yyyy}1231.nc
        if [ ! -f $outfile_accw ] || [ ! -f $outfile_co2 ] || [ ! -f $outfile_echam ] ; then
            miss_list_restart_echam="${miss_list_restart_echam} ${yyyy}"
            ((miss_counter_restart_echam++))
        fi
        # HAMOCC
        outfile_hamocc=${restartdir}/hamocc/rerun_${expid}_hamocc_${yyyy}1231.nc
        if [ ! -f $outfile_hamocc ] ; then
            miss_list_restart_hamocc="${miss_list_restart_hamocc} ${yyyy}"
            ((miss_counter_restart_hamocc++))
        fi
        # JSBACH
        outfile_hd=${restartdir}/jsbach/restart_${expid}_hd_${yyyy}1231.nc
        outfile_jsbach=${restartdir}/jsbach/restart_${expid}_jsbach_${yyyy}1231.nc
        outfile_surf=${restartdir}/jsbach/restart_${expid}_surf_${yyyy}1231.nc
        outfile_veg=${restartdir}/jsbach/restart_${expid}_veg_${yyyy}1231.nc
        outfile_yasso=${restartdir}/jsbach/restart_${expid}_yasso_${yyyy}1231.nc
        if [ ! -f $outfile_hd ] || [ ! -f $outfile_jsbach ] || [ ! -f $outfile_surf ] || [ ! -f $outfile_veg ] || [ ! -f $outfile_yasso ] ; then
            miss_list_restart_jsbach="${miss_list_restart_jsbach} ${yyyy}"
            ((miss_counter_restart_jsbach++))
        fi
        # MPIOM
        outfile_mpiom=${restartdir}/mpiom/rerun_${expid}_mpiom_${yyyy}1231.nc
        if [ ! -f $outfile_mpiom ] ; then
            miss_list_restart_mpiom="${miss_list_restart_mpiom} ${yyyy}"
            ((miss_counter_restart_mpiom++))
        fi
        # OASIS3MCT
        outfile_flxatmos=${restartdir}/oasis3mct/flxatmos_${expid}_${yyyy}1231.tar
        outfile_sstocean=${restartdir}/oasis3mct/sstocean_${expid}_${yyyy}1231.tar
        if [ ! -f $outfile_flxatmos ] || [ ! -f $outfile_sstocean ] ; then
            miss_list_restart_oasis="${miss_list_restart_oasis} ${yyyy}"
            ((miss_counter_restart_oasis++))
        fi
    done
    if [ $miss_counter_restart_echam -gt 0 ]; then
            echo ${miss_counter_restart_echam} missing ECHAM restart "file(s)": ${miss_list_restart_echam}
    fi
    if [ $miss_counter_restart_hamocc -gt 0 ]; then
            echo ${miss_counter_restart_hamocc} missing HAMOCC restart "file(s)": ${miss_list_restart_hamocc}
    fi
    if [ $miss_counter_restart_jsbach -gt 0 ]; then
            echo ${miss_counter_restart_jsbach} missing JSBACH restart "file(s)": ${miss_list_restart_jsbach}
    fi
    if [ $miss_counter_restart_mpiom -gt 0 ]; then
            echo ${miss_counter_restart_mpiom} missing MPIOM restart "file(s)": ${miss_list_restart_mpiom}
    fi
    if [ $miss_counter_restart_oasis -gt 0 ]; then
            echo ${miss_counter_restart_oasis} missing OASIS3MCT restart "file(s)": ${miss_list_restart_oasis}
    fi
    if [ $miss_counter_restart_echam -gt 0 ] || [ $miss_counter_restart_hamocc -gt 0 ] || [ $miss_counter_restart_jsbach -gt 0 ] || [ $miss_counter_restart_mpiom -gt 0 ] || [ $miss_counter_restart_oasis -gt 0 ]; then
        ((miss_counter_restart_all++))
    else
        echo restart files OK
    fi

    ###################
    # check output:
    miss_counter_output_echam=0
    miss_list_output_echam=" "
    miss_counter_output_hamocc=0
    miss_list_ouput_hamocc=" "
    miss_counter_output_jsbach=0
    miss_list_output_jsbach=" "
    miss_counter_output_mpiom=0
    miss_list_ouput_mpiom=" "
        for yyyy in $(seq $startyear $endyear); do
        # ECHAM
        outfile_accw=${outdir}/echam6/${expid}_echam6_accw_${yyyy}.grb
        outfile_co2=${outdir}/echam6/${expid}_echam6_co2_${yyyy}.grb
        outfile_co2mm=${outdir}/echam6/${expid}_echam6_co2_mm_${yyyy}.grb
        outfile_echam=${outdir}/echam6/${expid}_echam6_echam_${yyyy}.grb
        outfile_log=${outdir}/echam6/${expid}_echam6_LOG_mm_${yyyy}.grb
        if [ ! -f $outfile_accw ] || [ ! -f $outfile_co2 ] || [ ! -f $outfile_co2mm ] || [ ! -f $outfile_echam ] || [ ! -f $outfile_log ]  ; then
            miss_list_output_echam="${miss_list_output_echam} ${yyyy}"
            ((miss_counter_output_echam++))
        fi
        # HAMOCC
        outfile_hamocc_co2=${outdir}/hamocc/${expid}_hamocc_co2_${yyyy}0101_${yyyy}1231.nc
        outfile_hamocc_data_2d_mm=${outdir}/hamocc/${expid}_hamocc_data_2d_mm_${yyyy}0101_${yyyy}1231.nc
        if [ $hamocc_monthly -eq 1 ]; then
            outfile_hamocc_data_3d=${outdir}/hamocc/${expid}_hamocc_data_3d_mm_${yyyy}0101_${yyyy}1231.nc
        else
            outfile_hamocc_data_3d=${outdir}/hamocc/${expid}_hamocc_data_3d_ym_${yyyy}0101_${yyyy}1231.nc
        fi
        outfile_hamocc_data_eu_mm=${outdir}/hamocc/${expid}_hamocc_data_eu_mm_${yyyy}0101_${yyyy}1231.nc
        outfile_hamocc_sedi_ym=${outdir}/hamocc/${expid}_hamocc_data_sedi_ym_${yyyy}0101_${yyyy}1231.nc
        outfile_hamocc_monitoring_ym=${outdir}/hamocc/${expid}_hamocc_monitoring_ym_${yyyy}0101_${yyyy}1231.nc
        if [ ! -f $outfile_hamocc_co2 ] || [ ! -f $outfile_hamocc_data_2d_mm ] || [ ! -f $outfile_hamocc_data_3d ] || [ ! -f $outfile_hamocc_data_eu_mm ] || [ ! -f $outfile_hamocc_sedi_ym ] || [ ! -f $outfile_hamocc_monitoring_ym ]; then
            miss_list_output_hamocc="${miss_list_output_hamocc} ${yyyy}"
            ((miss_counter_output_hamocc++))
        fi
        # JSBACH
        outfile_jsbach_jsbach=${outdir}/jsbach/${expid}_jsbach_jsbach_${yyyy}.grb
        outfile_jsbach_jsbach_mm=${outdir}/jsbach/${expid}_jsbach_jsbach_mm_${yyyy}.grb
        outfile_jsbach_land=${outdir}/jsbach/${expid}_jsbach_land_${yyyy}.grb
        outfile_jsbach_land_mm=${outdir}/jsbach/${expid}_jsbach_land_mm_${yyyy}.grb
        outfile_jsbach_surf=${outdir}/jsbach/${expid}_jsbach_surf_${yyyy}.grb
        outfile_jsbach_surf_mm=${outdir}/jsbach/${expid}_jsbach_surf_mm_${yyyy}.grb
        outfile_jsbach_veg=${outdir}/jsbach/${expid}_jsbach_veg_${yyyy}.grb
        outfile_jsbach_veg_mm=${outdir}/jsbach/${expid}_jsbach_veg_mm_${yyyy}.grb
        outfile_jsbach_yasso=${outdir}/jsbach/${expid}_jsbach_yasso_${yyyy}.grb
        outfile_jsbach_yasso_mm=${outdir}/jsbach/${expid}_jsbach_yasso_mm_${yyyy}.grb

        if [ ! -f $outfile_jsbach_jsbach ] || [ ! -f $outfile_jsbach_jsbach_mm ] || [ ! -f $outfile_jsbach_land ] || [ ! -f $outfile_jsbach_land_mm ] || [ ! -f $outfile_jsbach_surf ] || [ ! -f $outfile_jsbach_surf_mm ] || [ ! -f $outfile_jsbach_veg ] || [ ! -f $outfile_jsbach_veg_mm ] || [ ! -f $outfile_jsbach_yasso ] || [ ! -f $outfile_jsbach_yasso_mm ]; then
            miss_list_output_jsbach="${miss_list_output_jsbach} ${yyyy}"
            ((miss_counter_output_jsbach++))
        fi
        # MPIOM
        outfile_mpiom_2d=${outdir}/mpiom/${expid}_mpiom_data_2d_mm_${yyyy}0101_${yyyy}1231.nc
        outfile_mpiom_3d=${outdir}/mpiom/${expid}_mpiom_data_3d_mm_${yyyy}0101_${yyyy}1231.nc
        outfile_mpiom_moc=${outdir}/mpiom/${expid}_mpiom_data_moc_mm_${yyyy}0101_${yyyy}1231.nc
        outfile_mpiom_monitoring=${outdir}/mpiom/${expid}_mpiom_monitoring_ym_${yyyy}0101_${yyyy}1231.nc
        outfile_mpiom_timeser=${outdir}/mpiom/${expid}_mpiom_timeser_mm_${yyyy}0101_${yyyy}1231.nc
        if [ ! -f $outfile_mpiom_2d ] || [ ! -f $outfile_mpiom_3d ] || [ ! -f $outfile_mpiom_moc ] || [ ! -f $outfile_mpiom_monitoring ] || [ ! -f $outfile_mpiom_timeser ]; then
            miss_list_output_mpiom="${miss_list_output_mpiom} ${yyyy}"
            ((miss_counter_output_mpiom++))
        fi
    done
    if [ $miss_counter_output_echam -gt 0 ]; then
            echo ${miss_counter_output_echam} missing ECHAM output "file(s)": ${miss_list_output_echam}
    fi
    if [ $miss_counter_output_hamocc -gt 0 ]; then
            echo ${miss_counter_output_hamocc} missing HAMOCC output "file(s)": ${miss_list_output_hamocc}
    fi
    if [ $miss_counter_output_jsbach -gt 0 ]; then
            echo ${miss_counter_output_jsbach} missing JSBACH output "file(s)": ${miss_list_output_jsbach}
    fi
    if [ $miss_counter_output_mpiom -gt 0 ]; then
            echo ${miss_counter_output_mpiom} missing MPIOM output "file(s)": ${miss_list_output_mpiom}
    fi

    if [ $miss_counter_output_echam -gt 0 ] || [ $miss_counter_output_hamocc -gt 0 ] || [ $miss_counter_output_jsbach -gt 0 ] || [ $miss_counter_output_mpiom -gt 0 ]; then
        ((miss_counter_output_all++))
    else
        echo output files OK
    fi


    ###################
    # check postprocessed ECHAM files:

    # check for BOT files
    miss_counter_BOT=0
    miss_list_BOT=" "

    for yyyy in $(seq $startyear $endyear); do
    	outfile=${echamdir}/${expid}_echam6_BOT_mm_${yyyy}.grb
    	if [ ! -f $outfile ]; then
    		miss_list_BOT="${miss_list_BOT} ${yyyy}"
    		((miss_counter_BOT++))
    	fi
    done
    if [ $miss_counter_BOT -gt 0 ]; then
    		echo ${miss_counter_BOT} missing BOT "file(s)": ${miss_list_BOT}
    fi
    # check for ATM files
    miss_counter_ATM=0
    miss_list_ATM=" "

        for yyyy in $(seq $startyear $endyear); do
    	outfile=${echamdir}/${expid}_echam6_ATM_mm_${yyyy}.grb
    	if [ ! -f $outfile ]; then
    		miss_list_ATM="${miss_list_ATM} ${yyyy}"
    		((miss_counter_ATM++))
    	fi
    done
    if [ $miss_counter_ATM -gt 0 ]; then
    		echo ${miss_counter_ATM} missing ATM "file(s)": ${miss_list_ATM}
    fi
    if [ $miss_counter_BOT -gt 0 ] || [ $miss_counter_ATM -gt 0 ]; then
        ((miss_counter_pp_all++))
    else
        echo post-processed ECHAM files OK.
    fi

done

echo "================================="
echo Summary:
echo " "

    if [ $miss_counter_restart_all -eq 0 ]; then
        echo Restart files: OK
    else
        echo Restart files incomplete, ${miss_counter_restart_all} experiments have missing files
    fi

    if [ $miss_counter_output_all -eq 0 ]; then
        echo output files: OK
    else
        echo Output files incomplete, ${miss_counter_output_all} experiments have missing files
    fi

    if [ $miss_counter_pp_all -eq 0 ]; then
        echo Postprocessing: OK
    else
        echo Postprocessed output incomplete, ${miss_counter_pp_all} experiments have missing files
    fi
echo " "
echo "================================="
echo " "
echo " "

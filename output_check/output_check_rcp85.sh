#!/bin/bash

# performs integrity check on output (checks if files are present, mainly for BOT and ATM files)

# directory where experiments are stored, i.e. parent directory of ensemble realisations
expdir=/work/bm0976/m300497/MPIESM/GLENSv1/output/experiments #/work/mh0033/m214013/mpiesm-1.1.00p2/experiments
# prefix common to all experiments
prefix=cr #mb
suffix=_rcp85

# id of first and last experiment
run_0=101 #469
run_n=166 #500

# startyear and endyear of experiment
startyear=2006 #1850
endyear=2099 #1999

miss_counter_all=0
#============= Loop through each run
for rrr in $(seq $run_0 $run_n); do
	expid=${prefix}$(printf "%04d" $rrr)${suffix}
    echo "================================="
    echo ${expid}

    echamdir=${expdir}/${expid}/outdata/echam6
    # check for BOT files
    miss_counter=0
    for yyyy in $(seq $startyear $endyear); do
    	outfile=${echamdir}/${expid}_echam6_BOT_mm_${yyyy}.grb
    	if [ ! -f $outfile ]; then
    		echo BOT missing: ${yyyy}
    		((miss_counter++))
            ((miss_counter_all++))
    	fi
    done
    if [ $miss_counter -eq 0 ]; then
    		echo ${expid} BOT ok.
    	else
    		echo ${expid} missing BOT files: ${miss_counter}
    fi
    # check for ATM files
    miss_counter=0
    for yyyy in $(seq $startyear $endyear); do
    	outfile=${echamdir}/${expid}_echam6_ATM_mm_${yyyy}.grb
    	if [ ! -f $outfile ]; then
    		echo ATM missing: ${yyyy}
    		((miss_counter++))
            ((miss_counter_all++))
    	fi
    done
    if [ $miss_counter -eq 0 ]; then
    		echo ${expid} ATM ok.
    	else
    		echo ${expid} missing ATM files: ${miss_counter}
    fi
done

    if [ $miss_counter_all -eq 0 ]; then
        echo Output complete, no missing files found.
    else
        echo Output incomplete, total number of missing files: $miss_counter_all
    fi
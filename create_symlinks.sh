#!/bin/bash

# v1.0 (5.1.2018 by sebastian.milinski@mpimet.mpg.de)

# TODO: check if links exist and either delete them or don't recreate (otherwise there are lots of warnings) (use ln -sf but this doesn't )
# TODO: add underscore between prefix and numeric id (for some reason this gives warnings when trying to create symlinks)
# TODO: parallelisation: limit to n_max parallel jobs?

# TODO: implement dynamic linking of BOT and ATM files in case of missing files (see issue #1)

# This script creates symbolic links to all model output. During this process it:
# - copies the whole directory structure from the source directory to the target directory
# - creates symbolic links for all files present in the source directory in the same location in the target directory
# - allows to rename the links to files in order to change the experiment id

# The aim of this is to have one directory for the Grand Ensemble that holds subfolders for each experiment (piControl, historical, 1% CO2, RCP2.6, RCP4.5 and RCP8.5)

# Necessary user input (* :defaults already set based on output location and data availability on mistral on 5.1.2018):
# - target directory
# - * source directory
# - * experiment id (source)
# - * experiment id (target)

target_dir=/work/mh1007/MPI-GE # creates target directory

#####
# control if all experiments should be processed
force_historical=false
force_1perc=false
force_rcp26=true
force_rcp45=true
force_rcp85=false
force_control=false


#####################
# PiControl
if [ "$force_control" = true ]; then

echo creating links for PiControl runs


# path to control run from swiss machine
source_dir=/work/mh1007/mpiesm1/experiments

# directory where links to members will be created. Choose a meaningful name that starts with a letter
target_subdir=${target_dir}/pictrl

# which members should be used from source_dir? Define prefix and counter (counter is expanded to 4-digit number with leading zeros later)
source_prefix=lkm
ens_0=0001 		# first ensemble member to include
ens_n=${ens_0}  # last ensemble member to include

# source_prefic will be substituted with this
target_prefix=pictrl
# ensemble counter offset, i.e. choose -100 to change lkm0101 to hist0001
counter_offset=0

# Loop through each ensemble member
for rrr in $(seq $ens_0 $ens_n); do #change to 200
	source_expid=${source_prefix}$(printf "%04d" $rrr)
	target_counter=$(expr $rrr + $counter_offset)
	target_expid=${target_prefix}$(printf "%04d" $target_counter)
	echo $source_expid - $target_expid
	# echo copy directory structure
	mkdir -p ${target_subdir}/${target_expid}
	find ${source_dir}/${source_expid} -mindepth 1 -depth -type d -printf "%P\n" | while read dir; do mkdir -p "${target_subdir}/${target_expid}/${dir}"; done
	# echo linking files
	find ${source_dir}/${source_expid} -type f -printf "%P\n" | while read file; do ln -s ${source_dir}/${source_expid}/$file $(echo ${target_subdir}/${target_expid}/$file | sed "s/$source_expid/$target_expid/"); done &
done
wait


# path to control run from mistral
source_dir=/work/mh0469/m214089/mpiesm/experiments

# directory where links to members will be created. Choose a meaningful name that starts with a letter
target_subdir=${target_dir}/pictrl

# which members should be used from source_dir? Define prefix and counter (counter is expanded to 4-digit number with leading zeros later)
source_prefix=lkm
ens_0=0016 		# first ensemble member to include
ens_n=${ens_0}  # last ensemble member to include

# source_prefic will be substituted with this
target_prefix=pictrl
# ensemble counter offset, i.e. choose -100 to change lkm0101 to hist0001
counter_offset=-14

# Loop through each ensemble member
for rrr in $(seq $ens_0 $ens_n); do #change to 200
	source_expid=${source_prefix}$(printf "%04d" $rrr)
	target_counter=$(expr $rrr + $counter_offset)
	target_expid=${target_prefix}$(printf "%04d" $target_counter)
	echo $source_expid - $target_expid
	# echo copy directory structure
	mkdir -p ${target_subdir}/${target_expid}
	find ${source_dir}/${source_expid} -mindepth 1 -depth -type d -printf "%P\n" | while read dir; do mkdir -p "${target_subdir}/${target_expid}/${dir}"; done
	# echo linking files
	find ${source_dir}/${source_expid} -type f -printf "%P\n" | while read file; do ln -s ${source_dir}/${source_expid}/$file $(echo ${target_subdir}/${target_expid}/$file | sed "s/$source_expid/$target_expid/"); done &
done
wait
fi


#####################
# historical
if [ "$force_historical" = true ]; then
# path to directory where all members for the experiment are located
source_dir=/work/mh1007/mpiesm1/experiments

# directory where links to members will be created. Choose a meaningful name that starts with a letter
target_subdir=${target_dir}/hist

# which members should be used from source_dir? Define prefix and counter (counter is expanded to 4-digit number with leading zeros later)
source_prefix=lkm
ens_0=101 		# first ensemble member to include
ens_n=200		# last ensemble member to include

# source_prefic will be substituted with this
target_prefix=hist
# ensemble counter offset, i.e. choose -100 to change lkm0101 to hist0001
counter_offset=-100


echo creating links for historical runs
# Loop through each ensemble member
for rrr in $(seq $ens_0 $ens_n); do #change to 200
	source_expid=${source_prefix}$(printf "%04d" $rrr)
	target_counter=$(expr $rrr + $counter_offset)
	target_expid=${target_prefix}$(printf "%04d" $target_counter)
	echo $source_expid - $target_expid
	# echo copy directory structure
	mkdir -p ${target_subdir}/${target_expid}
	find ${source_dir}/${source_expid} -mindepth 1 -depth -type d -printf "%P\n" | while read dir; do mkdir -p "${target_subdir}/${target_expid}/${dir}"; done
	# echo linking files
	find ${source_dir}/${source_expid} -type f -printf "%P\n" | while read file; do ln -s ${source_dir}/${source_expid}/$file $(echo ${target_subdir}/${target_expid}/$file | sed "s/$source_expid/$target_expid/"); done &
done
wait
fi

#####################
# 1% CO2 #1-68
if [ "$force_1perc" = true ]; then
# path to directory where all members for the experiment are located
source_dir=/work/mh1007/mpiesm1/experiments

# directory where links to members will be created. Choose a meaningful name that starts with a letter
target_subdir=${target_dir}/onepct

# which members should be used from source_dir? Define prefix and counter (counter is expanded to 4-digit number with leading zeros later)
source_prefix=lkm
ens_0=401 		# first ensemble member to include
ens_n=468		# last ensemble member to include

# source_prefic will be substituted with this
target_prefix=onepct
# ensemble counter offset, i.e. choose -100 to change lkm0101 to hist0001
counter_offset=-400


echo creating links for 1%CO2 runs 1-68
# Loop through each ensemble member
for rrr in $(seq $ens_0 $ens_n); do #change to 200
	source_expid=${source_prefix}$(printf "%04d" $rrr)
	target_counter=$(expr $rrr + $counter_offset)
	target_expid=${target_prefix}$(printf "%04d" $target_counter)
	echo $source_expid - $target_expid
	# echo copy directory structure
	mkdir -p ${target_subdir}/${target_expid}
	find ${source_dir}/${source_expid} -mindepth 1 -depth -type d -printf "%P\n" | while read dir; do mkdir -p "${target_subdir}/${target_expid}/${dir}"; done
	# echo linking files
	find ${source_dir}/${source_expid} -type f -printf "%P\n" | while read file; do ln -s ${source_dir}/${source_expid}/$file $(echo ${target_subdir}/${target_expid}/$file | sed "s/$source_expid/$target_expid/"); done &
done
wait
#####################
# 1% CO2 #68-100

# path to directory where all members for the experiment are located
source_dir=/work/mh0033/m214013/mpiesm-1.1.00p2/experiments

# directory where links to members will be created. Choose a meaningful name that starts with a letter
target_subdir=${target_dir}/onepct

# which members should be used from source_dir? Define prefix and counter (counter is expanded to 4-digit number with leading zeros later)
source_prefix=mb
ens_0=469 		# first ensemble member to include
ens_n=500		# last ensemble member to include

# source_prefic will be substituted with this
target_prefix=onepct
# ensemble counter offset, i.e. choose -100 to change lkm0101 to hist0001
counter_offset=-400


echo creating links for 1%CO2 runs 69-100
# Loop through each ensemble member
for rrr in $(seq $ens_0 $ens_n); do #change to 200
	source_expid=${source_prefix}$(printf "%04d" $rrr)
	target_counter=$(expr $rrr + $counter_offset)
	target_expid=${target_prefix}$(printf "%04d" $target_counter)
	echo $source_expid - $target_expid
	# echo copy directory structure
	mkdir -p ${target_subdir}/${target_expid}
	find ${source_dir}/${source_expid} -mindepth 1 -depth -type d -printf "%P\n" | while read dir; do mkdir -p "${target_subdir}/${target_expid}/${dir}"; done
	# echo linking files
	find ${source_dir}/${source_expid} -type f -printf "%P\n" | while read file; do ln -s ${source_dir}/${source_expid}/$file $(echo ${target_subdir}/${target_expid}/$file | sed "s/$source_expid/$target_expid/"); done &
done
wait
fi
#####################
# RCP 2.6
if [ "$force_rcp26" = true ]; then
# path to directory where all members for the experiment are located
source_dir=/work/mh1007/mpiesm1/experiments

# directory where links to members will be created. Choose a meaningful name that starts with a letter
target_subdir=${target_dir}/rcp26

# which members should be used from source_dir? Define prefix and counter (counter is expanded to 4-digit number with leading zeros later)
source_prefix=mb
source_suffix=_rcp26
ens_0=101 		# first ensemble member to include
ens_n=200		# last ensemble member to include

# source_prefic will be substituted with this
target_prefix=rcp26
# ensemble counter offset, i.e. choose -100 to change lkm0101 to hist0001
counter_offset=-100


echo creating links for RCP 2.6 runs
# Loop through each ensemble member
for rrr in $(seq $ens_0 $ens_n); do #change to 200
	source_expid=${source_prefix}$(printf "%04d" $rrr)${source_suffix}
	target_counter=$(expr $rrr + $counter_offset)
	target_expid=${target_prefix}$(printf "%04d" $target_counter)
	echo $source_expid - $target_expid
	# echo copy directory structure
	mkdir -p ${target_subdir}/${target_expid}
	find ${source_dir}/${source_expid} -mindepth 1 -depth -type d -printf "%P\n" | while read dir; do mkdir -p "${target_subdir}/${target_expid}/${dir}"; done
	# echo linking files
	find ${source_dir}/${source_expid} -type f -printf "%P\n" | while read file; do ln -s ${source_dir}/${source_expid}/$file $(echo ${target_subdir}/${target_expid}/$file | sed "s/$source_expid/$target_expid/"); done &
done
wait
fi
#####################
# RCP 4.5
if [ "$force_rcp45" = true ]; then
# path to directory where all members for the experiment are located
source_dir=/work/mh1007/mpiesm1/experiments

# directory where links to members will be created. Choose a meaningful name that starts with a letter
target_subdir=${target_dir}/rcp45

# which members should be used from source_dir? Define prefix and counter (counter is expanded to 4-digit number with leading zeros later)
source_prefix=mb
source_suffix=_rcp45
ens_0=101 		# first ensemble member to include
ens_n=200		# last ensemble member to include

# source_prefic will be substituted with this
target_prefix=rcp45
# ensemble counter offset, i.e. choose -100 to change lkm0101 to hist0001
counter_offset=-100


echo creating links for RCP 4.5 runs
# Loop through each ensemble member
for rrr in $(seq $ens_0 $ens_n); do #change to 200
	source_expid=${source_prefix}$(printf "%04d" $rrr)${source_suffix}
	target_counter=$(expr $rrr + $counter_offset)
	target_expid=${target_prefix}$(printf "%04d" $target_counter)
	echo $source_expid - $target_expid
	# echo copy directory structure
	mkdir -p ${target_subdir}/${target_expid}
	find ${source_dir}/${source_expid} -mindepth 1 -depth -type d -printf "%P\n" | while read dir; do mkdir -p "${target_subdir}/${target_expid}/${dir}"; done
	# echo linking files
	find ${source_dir}/${source_expid} -type f -printf "%P\n" | while read file; do ln -s ${source_dir}/${source_expid}/$file $(echo ${target_subdir}/${target_expid}/$file | sed "s/$source_expid/$target_expid/"); done &
done
wait
fi

#####################
# RCP 8.5 1-100
if [ "$force_rcp85" = true ]; then
# path to directory where all members for the experiment are located
source_dir=/work/mh1007/mpiesm1/experiments

# directory where links to members will be created. Choose a meaningful name that starts with a letter
target_subdir=${target_dir}/rcp85

# which members should be used from source_dir? Define prefix and counter (counter is expanded to 4-digit number with leading zeros later)
source_prefix=cr
source_suffix=_rcp85
ens_0=101 		# first ensemble member to include
ens_n=200		# last ensemble member to include

# source_prefix will be substituted with this
target_prefix=rcp85
# ensemble counter offset, i.e. choose -100 to change lkm0101 to hist0001
counter_offset=-100


echo creating links for RCP 8.5 runs
# Loop through each ensemble member
for rrr in $(seq $ens_0 $ens_n); do #change to 200
	source_expid=${source_prefix}$(printf "%04d" $rrr)${source_suffix}
	target_counter=$(expr $rrr + $counter_offset)
	target_expid=${target_prefix}$(printf "%04d" $target_counter)
	echo $source_expid - $target_expid
	# echo copy directory structure
	mkdir -p ${target_subdir}/${target_expid}
	find ${source_dir}/${source_expid} -mindepth 1 -depth -type d -printf "%P\n" | while read dir; do mkdir -p "${target_subdir}/${target_expid}/${dir}"; done
	# echo linking files
	find ${source_dir}/${source_expid} -type f -printf "%P\n" | while read file; do ln -s ${source_dir}/${source_expid}/$file $(echo ${target_subdir}/${target_expid}/$file | sed "s/$source_expid/$target_expid/"); done &
done
wait
fi

#####################
# # RCP 8.5 61-80
# if [ "$force_rcp85" = true ]; then
# # path to directory where all members for the experiment are located
# source_dir=/work/bm0976/m300497/MPIESM/GLENSv1/output/experiments

# # directory where links to members will be created. Choose a meaningful name that starts with a letter
# target_subdir=${target_dir}/rcp85

# # which members should be used from source_dir? Define prefix and counter (counter is expanded to 4-digit number with leading zeros later)
# source_prefix=cr
# source_suffix=_rcp85
# ens_0=161 		# first ensemble member to include
# ens_n=180		# last ensemble member to include

# # source_prefix will be substituted with this
# target_prefix=rcp85
# # ensemble counter offset, i.e. choose -100 to change lkm0101 to hist0001
# counter_offset=-100


# echo creating links for RCP 8.5 runs
# # Loop through each ensemble member
# for rrr in $(seq $ens_0 $ens_n); do #change to 200
# 	source_expid=${source_prefix}$(printf "%04d" $rrr)${source_suffix}
# 	target_counter=$(expr $rrr + $counter_offset)
# 	target_expid=${target_prefix}$(printf "%04d" $target_counter)
# 	echo $source_expid - $target_expid
# 	# echo copy directory structure
# 	mkdir -p ${target_subdir}/${target_expid}
# 	find ${source_dir}/${source_expid} -mindepth 1 -depth -type d -printf "%P\n" | while read dir; do mkdir -p "${target_subdir}/${target_expid}/${dir}"; done
# 	# echo linking files
# 	find ${source_dir}/${source_expid} -type f -printf "%P\n" | while read file; do ln -s ${source_dir}/${source_expid}/$file $(echo ${target_subdir}/${target_expid}/$file | sed "s/$source_expid/$target_expid/"); done &
# done
# wait
# fi







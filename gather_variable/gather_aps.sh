#!/bin/bash

script=/home/mpim/m300265/scripts/MPI-ESM-GE/gather_variable/gather_variables.sh
outdir=/work/mh1007/MPI-GE_processed
# This script only invokes the gather_variable.sh script.
# common variables
bash ${script} -e rcp85 -o ${outdir} -v aps
bash ${script} -e hist -o ${outdir} -v aps
bash ${script} -e rcp26 -o ${outdir} -v aps
bash ${script} -e rcp45 -o ${outdir} -v aps
bash ${script} -e onepct -o ${outdir} -v aps
bash ${script} -e pictrl -o ${outdir} -v aps -r0 1
bash ${script} -e pictrl -o ${outdir} -v aps -r0 2

#!/bin/bash

# this script only performs the merge step from the gather_6h_precip.sh script.
# It should not be necessary to run this script. Only if merging was deactivated or failed, running only this step saves a lot of time.

ensfirst=69
enslast=100

# years: 1850 to 1999
#
yrfirst=1850
yrlast=1999

dirout=/work/mh0033/m300265/processed_thesis/onepct/hf_output

var=precip

dir_final=/work/mh1007/MPI-GE_processed/onepct/hf_output/${var}

echo "Merge years into single file..."

for ensdesc in $(seq -f "%04g" ${ensfirst} 1 ${enslast}); do
    echo "member: $ensdesc"
    ifiles=${dirout}/onepct-${var}_${ensdesc}_????.grb
    ofile=${dir_final}/onepct_${yrfirst}-${yrlast}_ens_${ensdesc}.${var}.nc
    cdo -f nc -t echam6 mergetime $ifiles $ofile
done

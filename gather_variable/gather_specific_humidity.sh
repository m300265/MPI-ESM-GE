#!/bin/bash

# version: 5.3.2018
# contribution from Sebastian Milinski and Dian Putrasahan

# extract selected variables from range of experiments
# merge all ensemble members into one file with dimensions (ens|time|lat|lon)

# currently using ATM files to extract specific humidity
# TODO:
#       - option to choose between ocean and atmosphere (and 2d/3d)
#       - parallelise


module load nco
module load cdo

# directory with symlinks to all runs
basedir=/work/mh1007/MPI-ESM-GE
# this is where the processed file will go
workdir=/scratch/m/$USER
outdir=${workdir}/${experiment}

# this will be used as a temporary storage for some intermediate processing steps.
# can be deleted afterwards (not automatically deleted by this script)
tmpdir=/scratch/m/$USER/tmp/${experiment}


### Which experiment and runs do you want?
experiment=onepct # hist | onepct | rcp26 | rcp45 | rcp85
#first run in sequence
run_0=1
#last run in sequence
run_n=100

# Which years should be included? (Note that output for some 1% runs is only available for 1850-1999)
yearstart=1850
yearend=1999

# comma separated list of variables
# make sure that variables are in the files that are being used (currently echam ATM)
# change datdir and ifiles below if you want to use different files
vars=q




# replace , with _ so that variable list can be used in filename
varstring=$(echo $vars | sed "s/,/_/g")

mkdir -p ${outdir}


#============= Loop through each run
for rrr in $(seq $run_0 $run_n); do #change to 200

    expid=${experiment}$(printf "%04d" $rrr)
    echo ${expid}
    ###### I/O
    datdir=${basedir}/${experiment}/${expid}/outdata/echam6
    curtmpdir=${tmpdir}/${expid}

    mkdir -p ${curtmpdir}


#     ####### processing
    ifiles=${datdir}/${expid}_echam6_ATM_mm_????.grb
    merged=${curtmpdir}/${expid}_echam6_${varstring}_mm.nc
    vertsum=${curtmpdir}/${expid}_echam6_${varstring}_mm_vertsum.nc
    if [ ! -f $vertsum ]; then
        cdo -t echam6 -f nc select,name=${vars},year=${yearstart}/${yearend} ${ifiles} ${merged}
        cdo vertsum ${merged} ${vertsum}
        rm ${merged}
    else
        echo "processed file found, skipping processing step 1"
    fi

done

# merge ensemble into one file
ensfile=${outdir}/${varstring}_${experiment}_${yearstart}-${yearend}.nc
if [ ! -f $ensfile ]; then
    #concatenate them and introduces a new record dimension called ens (order matters!)
    ncecat -O -u ens ${tmpdir}/*/*.nc $ensfile
    #Switch record and time dimension (so that time gets unlimited and record gets fixed)
    ncpdq -O -a time,ens $ensfile $ensfile
else
    echo "merged ensemble file found, skipping step"
fi


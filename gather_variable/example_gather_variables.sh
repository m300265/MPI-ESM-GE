#!/bin/bash

script=/home/mpim/m300265/scripts/MPI-ESM-GE/gather_variable/gather_variables.sh
outdir=/work/mh1007/MPI-GE_processed
# This script only invokes the gather_variable.sh script.
# common variables
bash ${script} -e rcp85 -o ${outdir} -v tsurf,u10,v10,wind10,precip,slp,qvi
bash ${script} -e hist -o ${outdir} -v tsurf,u10,v10,wind10,precip,slp,qvi
bash ${script} -e rcp26 -o ${outdir} -v tsurf,u10,v10,wind10,precip,slp,qvi
bash ${script} -e rcp45 -o ${outdir} -v tsurf,u10,v10,wind10,precip,slp,qvi
bash ${script} -e onepct -o ${outdir} -v tsurf,u10,v10,wind10,precip,slp,qvi
bash ${script} -e pictrl -o ${outdir} -v tsurf,u10,v10,wind10,precip,slp,qvi -r0 1
bash ${script} -e pictrl -o ${outdir} -v tsurf,u10,v10,wind10,precip,slp,qvi -r0 2


# fluxes
bash ${script} -e rcp85 -o ${outdir} -v ahfs,ahfl,srads,trads,srad0,trad0
bash ${script} -e hist -o ${outdir} -v ahfs,ahfl,srads,trads,srad0,trad0
bash ${script} -e rcp26 -o ${outdir} -v ahfs,ahfl,srads,trads,srad0,trad0
bash ${script} -e rcp45 -o ${outdir} -v ahfs,ahfl,srads,trads,srad0,trad0
bash ${script} -e onepct -o ${outdir} -v ahfs,ahfl,srads,trads,srad0,trad0
bash ${script} -e pictrl -o ${outdir} -v ahfs,ahfl,srads,trads,srad0,trad0 -r0 1
bash ${script} -e pictrl -o ${outdir} -v ahfs,ahfl,srads,trads,srad0,trad0 -r0 2

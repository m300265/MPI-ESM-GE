#!/bin/bash

# performs integrity check on output (checks if files are present, mainly for BOT and ATM files)

# directory where experiments are stored, i.e. parent directory of ensemble realisations
expdir=/work/mh1007/MPI-GE/onepct
# prefix common to all experiments
prefix=onepct
suffix=

# directory containing post-processed files that are missing in the original directory
# only used if ATM or BOT file is missing
backup_dir=/work/mh0033/m300466/mpiesm-1.1.00p2/experiments
source_prefix=mb
target_prefix=onepct
# ensemble counter offset, i.e. choose -100 to change lkm0101 to hist0001
counter_offset=-400
# if this is set to true, links are created. Otherwise the script only prints missing files
fill_gaps=true

# id of first and last experiment (combines lkm and mb 1% runs)
run_0=69  #1
run_n=100  #100

# startyear and endyear of experiment
startyear=1850
endyear=2005

miss_counter_all=0
#============= Loop through each run
for rrr in $(seq $run_0 $run_n); do
    expid=${prefix}$(printf "%04d" $rrr)${suffix}
    echo "================================="
    echo ${expid}
    if [ $rrr -ge 69 ]; then
        endyear=1999 # output for most mb runs only available until 1999
    fi
    echamdir=${expdir}/${expid}/outdata/echam6
    # check for BOT files
    miss_counter=0
    for yyyy in $(seq $startyear $endyear); do
        outfile=${echamdir}/${expid}_echam6_BOT_mm_${yyyy}.grb
        if [ ! -f $outfile ]; then
            echo BOT missing: ${yyyy}
            source_counter=$(expr $rrr - $counter_offset)
            backup_expid=${source_prefix}$(printf "%04d" $source_counter)
            backup_file=${backup_dir}/${backup_expid}/outdata/echam6/${backup_expid}_echam6_BOT_mm_${yyyy}.grb
            echo trying to link to ${backup_file}
            if [ "$fill_gaps" = true ] && [ -f ${backup_file} ]; then
                echo linking file...
                ln -s ${backup_file} ${outfile}
            else
                echo linking disabled or backup file not found. Doing nothing...
                ((miss_counter++))
                ((miss_counter_all++))
            fi
        fi
    done
    if [ $miss_counter -eq 0 ]; then
            echo ${expid} BOT ok.
        else
            echo ${expid} missing BOT files: ${miss_counter}
    fi
    # check for ATM files
    miss_counter=0
    for yyyy in $(seq $startyear $endyear); do
        outfile=${echamdir}/${expid}_echam6_ATM_mm_${yyyy}.grb
        if [ ! -f $outfile ]; then
            echo ATM missing: ${yyyy}
            source_counter=$(expr $rrr - $counter_offset)
            backup_expid=${source_prefix}$(printf "%04d" $source_counter)
            backup_file=${backup_dir}/${backup_expid}/outdata/echam6/${backup_expid}_echam6_ATM_mm_${yyyy}.grb
            echo trying to link to ${backup_file}
            if [ "$fill_gaps" = true ] && [ -f ${backup_file} ]; then
                echo linking file...
                ln -s ${backup_file} ${outfile}
            else
                echo linking disabled or backup file not found. Doing nothing...
                ((miss_counter++))
                ((miss_counter_all++))
            fi
        fi
    done
    if [ $miss_counter -eq 0 ]; then
            echo ${expid} ATM ok.
        else
            echo ${expid} missing ATM files: ${miss_counter}
    fi
done

    if [ $miss_counter_all -eq 0 ]; then
        echo Output complete, no missing files found.
    else
        echo Output incomplete, total number of missing files: $miss_counter_all
    fi

# MPI-ESM-GE toolbox

## Purpose
This is a collection of scripts facilitating simulation and analysis of large ensemble simulations with the MPI-ESM.

## Features
* create symlinks: create directory structure containing all experiments of the Grand Ensemble in one place
* output check: checks for missing years in model output or missing years in post-processed output


## General Information
More information on the MPI-ESM Grand Ensemble can be found in the wiki: ()

Data is stored on mistral: (https://wiki.mpimet.mpg.de/doku.php?id=models:mpi_esm:grand_ensemble:start)
```
/work/mh0033/m300265/MPI-ESM-GE (directory contains links to actual output)
```

